using Random

function tafel(; width=800, height=500)

    canvas_id = "tafel_canvas_$(randstring(15))"

    tafel_html = HTML("""
    <canvas id="$canvas_id" width="$width" height="$height"
            style="border-style: dashed; border-color: white; border-width: 1px"></canvas>
    <script>
    function distanceBetween(point1, point2) {
      return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
    }
    function angleBetween(point1, point2) {
      return Math.atan2( point2.x - point1.x, point2.y - point1.y );
    }
            
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width,
            y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
        };
    }
    
    var el = document.getElementById('$canvas_id');
    var rect = el.getBoundingClientRect();
    var ctx = el.getContext('2d');
    ctx.lineJoin = ctx.lineCap = 'round';
    
    var isDrawing, lastPoint;
    
    el.onmousedown = function(e) {
      isDrawing = true;
      lastPoint = getMousePos(el, e);
    };
    
    el.onmousemove = function(e) {
      if (!isDrawing) return;
      
      var currentPoint = getMousePos(el, e);
      var dist = distanceBetween(lastPoint, currentPoint);
      var angle = angleBetween(lastPoint, currentPoint);
      
      for (var i = 0; i < dist; i+=0.5) {
        
        x = lastPoint.x + (Math.sin(angle) * i);
        y = lastPoint.y + (Math.cos(angle) * i);
        
        var radgrad = ctx.createRadialGradient(x,y,1,x,y,2);
        
        radgrad.addColorStop(0, 'rgba(200,200,200,1.0)');
        radgrad.addColorStop(0.5, 'rgba(200,200,200,0.5)');
        radgrad.addColorStop(1, 'rgba(200,200,200,0.0)');
        
        ctx.fillStyle = radgrad;
         ctx.fillRect(x-2, y-2, 4, 4);
      }
      
      lastPoint = currentPoint;
    };
    
    el.onmouseup = function() {
      isDrawing = false;
    };
    </script>
    """)

    return tafel_html

end