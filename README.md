[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/NStiehm%2Fepidemiology-seminar/HEAD)

# Epidemiology Seminar

Seminar talk for the 'physics of socio-econmomical systems' course at TU Ilmenau

A small helper function to get an interactive drawing canvas is defined in Tafel.jl. You can either clone the repo, start a jupyter server and activate the julia environment (note that you will have to install the WebIO extension for jupyter)

```julia
julia>]activate .
```

or you can click the 'launch binder' badge at the top to start a jupyter lab session and scroll through the notebook right in your browser.
